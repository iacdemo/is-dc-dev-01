data "consul_keys" "extsw_internet_defaults" {
  key {
    name = "defaults"
    path = "iacdemo/configs/defaults/extsw_internet"
  }
}

data "consul_keys" "extsw_internet_interface" {
  key {
    name = "remote_as"
    path = "iacdemo/configs/${terraform.workspace}/extsw/internet/remote_as"
  }
  key {
    name = "subnet"
    path = "iacdemo/configs/${terraform.workspace}/extsw/internet/subnet"
  }
  key {
    name = "vlanid"
    path = "iacdemo/configs/${terraform.workspace}/extsw/internet/vlanid"
  }
}

locals {
  extsw_internet_1 = {
    interface = {
      ipv4        = "${cidrhost(data.consul_keys.extsw_internet_interface.var.subnet, -3)}/${split("/", data.consul_keys.extsw_internet_interface.var.subnet)[1]}"
      vlanid      = tonumber(data.consul_keys.extsw_internet_interface.var.vlanid)
      description = "${terraform.workspace}-EXT-TN"
      shutdown    = false
    }
    bgp_neighbor = {
      bgp_peer             = cidrhost(data.consul_keys.extsw_internet_interface.var.subnet, 1)
      router_as            = 65421
      remote_as            = tonumber(data.consul_keys.extsw_internet_interface.var.remote_as)
      default_originate    = true
      shutdown             = false
      soft_reconfiguration = "inbound"
      remove_private_as    = true
    }
    prefix_lists = [
      {
        name      = "pl_public_only"
        direction = "in"
      },
      {
        name      = "pl_default_only"
        direction = "out"
      }
    ]
  }
}