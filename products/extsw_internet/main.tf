module "extsw_internet_1" {
  source = "../../modules/terraform-iosxe-interconnect-bgp"

  interface    = local.extsw_internet_1.interface
  bgp_neighbor = local.extsw_internet_1.bgp_neighbor
  prefix_lists = toset(local.extsw_internet_1.prefix_lists)
}