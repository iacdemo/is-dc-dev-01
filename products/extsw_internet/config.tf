terraform {
  required_providers {
    iosxe = {
      source  = "poroping/iosxe"
      version = ">= 0.0.1"
    }
  }
}

