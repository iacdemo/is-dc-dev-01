module "aci_tenant" {
  source  = "qzx/tenant/aci"
  version = "1.2.3"

  tenant_name          = local.aci_tenant.tenant_name
  vrfs                 = local.aci_tenant.vrfs
  bridge_domains       = local.aci_tenant.bridge_domains
  application_profiles = local.aci_tenant.application_profiles
  epgs                 = local.aci_tenant.epgs
}

module "aci_to_fortigate_l3out" {
  for_each = local.vdom_inteconnects
  source   = "qzx/l3out/aci"
  version  = "1.2.0"

  tenant_name = each.value.tenant_name
  name        = each.value.name
  vrf         = each.value.vrf
  vrf_id      = each.value.vrf_id
  l3_domain   = each.value.l3_domain

  paths = each.value.paths

  external_epgs         = merge(each.value.default_epg, each.value.external_epgs)
  router_id_as_loopback = each.value.use_router_id_as_loopback
  ospf_enable           = each.value.ospf_enable
  static_routes         = each.value.static_routes

  depends_on = [
    module.aci_tenant
  ]
}