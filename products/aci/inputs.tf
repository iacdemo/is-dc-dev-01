# Please completely rewrite me as well as rewriting the friggin templates

data "consul_keys" "aci_defaults" {
  key {
    name = "defaults"
    path = "iacdemo/configs/defaults/aci"
  }
}

### ACI Tenant base config
data "consul_keys" "aci_tenant" {
  key {
    name    = "managed"
    path    = "iacdemo/configs/${terraform.workspace}/aci/tenant"
    default = false
  }
}

data "consul_key_prefix" "aci_vrfs" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/aci/vrfs/"
}

data "consul_keys" "aci_vrfs" {
  for_each = toset([for path in(keys(data.consul_key_prefix.aci_vrfs.subkeys)) : split("/", path)[0]])
  key {
    name = "name"
    path = "iacdemo/configs/${terraform.workspace}/aci/vrfs/${each.key}/name"
  }

  key {
    name    = "id"
    path    = "iacdemo/configs/${terraform.workspace}/aci/vrfs/${each.key}/id"
    default = 1
  }
}

data "consul_key_prefix" "aci_bridge_domains" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/aci/bridge_domains/"
}

data "consul_keys" "aci_bridge_domains" {
  for_each = toset([for path in(keys(data.consul_key_prefix.aci_bridge_domains.subkeys)) : split("/", path)[0]])
  key {
    name = "name"
    path = "iacdemo/configs/${terraform.workspace}/aci/bridge_domains/${each.key}/name"
  }

  key {
    name = "routing"
    path = "iacdemo/configs/${terraform.workspace}/aci/bridge_domains/${each.key}/routing"
  }

  key {
    name = "vrf"
    path = "iacdemo/configs/${terraform.workspace}/aci/bridge_domains/${each.key}/vrf"
  }
}

data "consul_key_prefix" "aci_application_profiles" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/aci/application_profiles/"
}

data "consul_key_prefix" "aci_epgs" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/aci/epgs/"
}

data "consul_keys" "aci_epgs" {
  for_each = toset([for path in(keys(data.consul_key_prefix.aci_epgs.subkeys)) : split("/", path)[0]])
  key {
    name = "name"
    path = "iacdemo/configs/${terraform.workspace}/aci/epgs/${each.key}/name"
  }

  key {
    name = "application_profile"
    path = "iacdemo/configs/${terraform.workspace}/aci/epgs/${each.key}/application_profile"
  }

  key {
    name = "bridge_domain"
    path = "iacdemo/configs/${terraform.workspace}/aci/epgs/${each.key}/bridge_domain"
  }

  key {
    name = "vmm_domain"
    path = "iacdemo/configs/${terraform.workspace}/aci/epgs/${each.key}/vmm_domain"
    default = ""
  }
}

## L2
data "consul_key_prefix" "aci_vdom_l2" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/aci/interfaces/EPGs/1800F/"
}

data "consul_keys" "aci_vdom_l2" {
  for_each = toset([for path in(keys(data.consul_key_prefix.aci_vdom_l2.subkeys)) : split("/", path)[0]])
  key {
    name = "epg"
    path = "iacdemo/configs/${terraform.workspace}/aci/interfaces/EPGs/1800F/${each.key}/epg"
  }

  key {
    name = "application_profile"
    path = "iacdemo/configs/${terraform.workspace}/aci/interfaces/EPGs/1800F/${each.key}/application_profile"
  }

  key {
    name = "encap"
    path = "iacdemo/configs/${terraform.workspace}/aci/interfaces/EPGs/1800F/${each.key}/encap"
  }
  key {
    name    = "tenant_name"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/EPGs/1800F/${each.key}/aci_tenant"
    default = terraform.workspace
  }
}

locals {
  aci_vdom_l2_paths = [jsondecode(data.consul_keys.aci_defaults.var.defaults)["paths"]["ha-vpc"]["1800F"]["EPG"]["path"]]
}

locals {
  l2_vdom_map = { for interface in data.consul_keys.aci_vdom_l2 : join("::", [interface.var.application_profile, interface.var.epg]) => {
    vlan_id = split("-", data.consul_keys.aci_vdom_l2[interface.var.encap].var.encap)[1]
    domain  = jsondecode(data.consul_keys.aci_defaults.var.defaults)["paths"]["ha-vpc"]["1800F"]["EPG"]["physical_domain"]
    paths   = local.aci_vdom_l2_paths
    }
  }
}

locals {
  vrfs = toset([for path in(keys(data.consul_key_prefix.aci_vrfs.subkeys)) : split("/", path)[0]])
}

locals {
  application_profiles = toset([for path in(keys(data.consul_key_prefix.aci_application_profiles.subkeys)) : split("/", path)[0]])
}

locals {
  bridge_domains = { for bd in data.consul_keys.aci_bridge_domains : bd.var.name => {
    name    = data.consul_keys.aci_bridge_domains[bd.var.name].var.name
    routing = data.consul_keys.aci_bridge_domains[bd.var.name].var.routing
    vrf     = data.consul_keys.aci_bridge_domains[bd.var.name].var.vrf
    }
  }
}

locals {
  epgs = { for epg in data.consul_keys.aci_epgs : join("::", [epg.var.application_profile, epg.var.name]) => {
    name                = data.consul_keys.aci_epgs[join("::", [epg.var.application_profile, epg.var.name])].var.name
    application_profile = data.consul_keys.aci_epgs[join("::", [epg.var.application_profile, epg.var.name])].var.application_profile
    bridge_domain       = data.consul_keys.aci_epgs[join("::", [epg.var.application_profile, epg.var.name])].var.bridge_domain
    static_paths = contains(keys(local.l2_vdom_map), join("::", [epg.var.application_profile, epg.var.name])) ? [for path in local.l2_vdom_map[join("::", [epg.var.application_profile, epg.var.name])].paths : {
      vlan_id = local.l2_vdom_map[join("::", [epg.var.application_profile, epg.var.name])].vlan_id
      path    = path
    }] : []
    domains = flatten([
      contains(keys(local.l2_vdom_map), join("::", [epg.var.application_profile, epg.var.name])) ? [local.l2_vdom_map[join("::", [epg.var.application_profile, epg.var.name])].domain] : []
    ])
    }
  }
}

locals {
  aci_tenant = {
    tenant_name          = terraform.workspace
    vrfs                 = local.vrfs
    application_profiles = local.application_profiles
    bridge_domains       = tomap(local.bridge_domains)
    epgs                 = tomap(local.epgs)
  }
}


### ACI-HA-VPC for FortiGate VDOM
## L3
data "consul_key_prefix" "vdom_interconnects" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/"
}

data "consul_keys" "vdom_interconnects" {
  for_each = toset([for path in(keys(data.consul_key_prefix.vdom_interconnects.subkeys)) : split("/", path)[0]])

  key {
    name = "vrf"
    path = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/vrf"
  }
  key {
    name    = "name"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/name"
    default = each.key
  }
  key {
    name = "encap"
    path = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/vlanid"
  }
  key {
    name    = "mtu"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/mtu"
    default = 1500
  }
  key {
    name = "subnet"
    path = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/ipv4"
  }
  key {
    name    = "cidrmask"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/cidrmask"
    default = 29
  }
  key {
    name    = "suffix"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/l3_suffix"
    default = "FW"
  }
  key {
    name    = "tenant_name"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/aci_tenant"
    default = terraform.workspace
  }
  key {
    name    = "is_ospf_interface"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/is_ospf_interface"
    default = false
  }
  key {
    name    = "epgs"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/epgs"
    default = "{}"
  }
  key {
    name    = "static_routes"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/static_routes"
    default = "[]"
  }
  key {
    name    = "scope"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/1800F/${each.key}/aci_scope"
    default = "local"
  }
}

locals {
  vdom_interconnect_settings = jsondecode(data.consul_keys.aci_defaults.var.defaults)["paths"]["ha-vpc"]["1800F"]["L3Out"]
}

locals {
  vdom_inteconnects = { for interface in data.consul_keys.vdom_interconnects : interface.var.name => {
    tenant_name = data.consul_keys.vdom_interconnects[interface.var.name].var.tenant_name
    name        = join("-", [data.consul_keys.vdom_interconnects[interface.var.name].var.tenant_name, data.consul_keys.vdom_interconnects[interface.var.name].var.suffix])
    vrf         = data.consul_keys.vdom_interconnects[interface.var.name].var.vrf
    vrf_id      = tonumber(data.consul_keys.aci_vrfs[interface.var.vrf].var.id)
    paths = {
      primary = {
        name                = jsondecode(data.consul_keys.aci_defaults.var.defaults)["paths"]["ha-vpc"]["1800F"]["L3Out"]["primary"]["name"]
        pod_id              = jsondecode(data.consul_keys.aci_defaults.var.defaults)["paths"]["ha-vpc"]["1800F"]["L3Out"]["primary"]["pod_id"]
        nodes               = jsondecode(data.consul_keys.aci_defaults.var.defaults)["paths"]["ha-vpc"]["1800F"]["L3Out"]["primary"]["nodes"]
        is_vpc              = jsondecode(data.consul_keys.aci_defaults.var.defaults)["paths"]["ha-vpc"]["1800F"]["L3Out"]["primary"]["is_vpc"]
        mtu                 = jsondecode(data.consul_keys.aci_defaults.var.defaults)["paths"]["ha-vpc"]["1800F"]["L3Out"]["primary"]["mtu"]
        vlan_id             = data.consul_keys.vdom_interconnects[interface.var.name].var.encap
        interconnect_subnet = data.consul_keys.vdom_interconnects[interface.var.name].var.subnet
      }
    }
    l3_domain   = jsondecode(data.consul_keys.aci_defaults.var.defaults)["paths"]["ha-vpc"]["1800F"]["L3Out"]["l3_domain"]
    ospf_enable = data.consul_keys.vdom_interconnects[interface.var.name].var.is_ospf_interface
    external_epgs = {
      for k, epg in jsondecode(data.consul_keys.vdom_interconnects[interface.var.name].var.epgs) : k => {
        subnets = epg.subnets
        scope   = contains(keys(epg), "scope") ? epg.scope : ["import-security"]
      }
    }
    default_epg = {
      join("-", [data.consul_keys.vdom_interconnects[interface.var.name].var.suffix, "Subnets"]) = {
        subnets = ["0.0.0.0/0"]
        scope   = data.consul_keys.vdom_interconnects[interface.var.name].var.scope == "shared" ? ["shared-rtctrl", "shared-security", "import-security"] : ["import-security"]
      }
    }
    use_router_id_as_loopback = false
    static_routes = jsondecode(data.consul_keys.vdom_interconnects[interface.var.name].var.static_routes)
  } }
}


# OSPF


data "consul_key_prefix" "ospf_interfaces" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/aci/routing/ospf_interfaces/"
}

data "consul_keys" "ospf_interfaces" {
  for_each = toset([for path in(keys(data.consul_key_prefix.ospf_interfaces.subkeys)) : split("/", path)[0]])
  key {
    name    = "tenant_name"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/${each.key}/aci_tenant"
    default = terraform.workspace
  }
  key {
    name    = "name"
    path    = "iacdemo/configs/${terraform.workspace}/aci/interfaces/L3Out/${each.key}/name"
    default = each.key
  }
}

locals {
  ospf_interfaces = { for ospf in data.consul_keys.ospf_interfaces : ospf.var.name => {
    tenant_name = data.consul_keys.ospf_interfaces[ospf.var.name].var.tenant_name
    }
  }
}



