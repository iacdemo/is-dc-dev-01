output "l3out" {
  description = "Object containing interface configuration potentially of interest to other products."
  value       = module.aci_to_fortigate_l3out
}

# output "debug" {
#   value = local.objects
# }
