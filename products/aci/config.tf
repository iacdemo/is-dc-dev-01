terraform {
  required_providers {
    consul = {
      source  = "hashicorp/consul"
      version = ">= 2.11.0"
    }
    aci = {
      source  = "CiscoDevNet/aci"
      version = "~> 0.7.1"
    }
  }
}

