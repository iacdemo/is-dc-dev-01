module "vdom" {
  source = "../../modules/terraform-fortios-vdom"
  providers = {
    fortios.root = fortios.root
    fortios.vdom = fortios.vdom
  }
  managed_by_terraform_warning = local.shared.managed_by_terraform_warning
  vcluster_id                  = data.consul_keys.vdom.var.vcluster_id
  vdom_name                    = terraform.workspace
}

module "global_objects" {
  count  = data.consul_keys.vdom.var.install_global_objects == "true" ? 1 : 0
  source = "../../modules/terraform-fortios-objects"
  providers = {
    fortios = fortios.vdom
  }

  objects = local.global_objects

  depends_on = [
    module.vdom
  ]
}

module "loopbacks" {
  for_each = local.loopbacks
  source   = "../../modules/terraform-fortios-vdom-interfaces"
  providers = {
    fortios.root = fortios.root
    fortios.vdom = fortios.vdom
  }

  allowaccess      = each.value.allowaccess
  description      = each.value.description
  ipv4             = each.value.ipv4 == "" ? null : each.value.ipv4
  ipv6             = each.value.ipv6 == "" ? null : each.value.ipv6
  name             = each.value.name
  secondary_ip     = each.value.secondary_ip
  secondary_ips    = each.value.secondary_ips
  type             = "loopback"
  parent_interface = null
  vdom             = module.vdom.vdom.name
  vlanid           = null
}

module "vlans" {
  for_each = local.vlans
  source   = "../../modules/terraform-fortios-vdom-interfaces"
  providers = {
    fortios.root = fortios.root
    fortios.vdom = fortios.vdom
  }

  allowaccess      = each.value.allowaccess
  description      = each.value.description
  ipv4             = each.value.ipv4 == "" ? null : each.value.ipv4
  ipv6             = each.value.ipv6 == "" ? null : each.value.ipv6
  mtu              = each.value.mtu
  name             = each.value.name
  secondary_ip     = each.value.secondary_ip
  secondary_ips    = each.value.secondary_ips
  type             = "vlan"
  parent_interface = each.value.parent_interface
  vdom             = module.vdom.vdom.name
  vlanid           = each.value.vlanid
}

module "vpns" {
  for_each = local.vpns
  source   = "../../modules/terraform-fortios-s2s-vpn"
  providers = {
    fortios = fortios.vdom
  }

  comments              = each.value.comments
  local_peer_ip         = each.value.local_peer_ip
  local_peer_ipv6       = each.value.local_peer_ipv6
  interface_local_ip    = each.value.interface_local_ip
  interface_remote_ip   = each.value.interface_remote_ip
  interface_tcp_mss     = each.value.interface_tcp_mss
  ip_fragmentation      = each.value.ip_fragmentation
  ip_version            = each.value.ip_version
  parent_interface      = each.value.parent_interface
  psk                   = each.value.psk
  phase1_dpd            = each.value.phase1_dpd
  phase1_dhgrp          = each.value.phase1_dhgrp
  phase1_ike_version    = each.value.phase1_ike_version
  phase1_keylife        = each.value.phase1_keylife
  phase1_nattraversal   = each.value.phase1_nattraversal
  phase1_proposal       = each.value.phase1_proposal
  phase2_auto_negotiate = each.value.phase2_auto_negotiate
  phase2_dhgrp          = each.value.phase2_dhgrp
  phase2_keepalive      = each.value.phase2_keepalive
  phase2_keylife_type   = each.value.phase2_keylife_type
  phase2_keylifekbs     = each.value.phase2_keylifekbs
  phase2_keylifeseconds = each.value.phase2_keylifeseconds
  phase2_pfs            = each.value.phase2_pfs
  phase2_proposal       = each.value.phase2_proposal
  phase2_replay         = each.value.phase2_replay
  phase2_selectors      = each.value.phase2_selectors
  remote_peer_ip        = each.value.remote_peer_ip
  vpn_name              = each.value.name
  vdom                  = each.value.vdom

  depends_on = [
    module.loopbacks,
    module.vlans
  ]
}

module "zones" {
  for_each = local.zones
  source   = "../../modules/terraform-fortios-zones"
  providers = {
    fortios = fortios.vdom
  }

  intrazone_traffic = each.value.intrazone_traffic
  members           = each.value.members
  name              = each.value.name
}

module "static_routes" {
  for_each = local.ipv4_static_routes
  source   = "../../modules/terraform-fortios-static-routes"
  providers = {
    fortios = fortios.vdom
  }

  blackhole = each.value.blackhole
  comment   = each.value.comment
  device    = each.value.device
  distance  = each.value.distance
  dst       = each.value.dst
  gateway   = each.value.gateway
  priority  = each.value.priority
  status    = each.value.status
  sdwan     = each.value.sdwan
}

resource "fortios_router_static6" "static_routes_ipv6" {
  for_each = local.ipv6_static_routes
  provider = fortios.vdom

  blackhole = each.value.blackhole
  comment   = each.value.comment
  device    = each.value.device
  distance  = each.value.distance
  dst       = each.value.dst
  gateway   = each.value.gateway
  priority  = each.value.priority
  status    = each.value.status
  sdwan     = each.value.sdwan
}


module "ospf" {
  source = "../../modules/terraform-fortios-ospf"
  providers = {
    fortios = fortios.vdom
  }

  bfd                             = data.consul_keys.ospf_settings.var.bfd == "" ? null : data.consul_keys.ospf_settings.var.bfd
  default_information_metric      = try(tonumber(data.consul_keys.ospf_settings.var.default_information_metric), null)
  default_information_metric_type = data.consul_keys.ospf_settings.var.default_information_metric_type == "" ? null : data.consul_keys.ospf_settings.var.default_information_metric_type
  default_information_originate   = data.consul_keys.ospf_settings.var.default_information_originate == "" ? null : data.consul_keys.ospf_settings.var.default_information_originate
  default_metric                  = try(tonumber(data.consul_keys.ospf_settings.var.default_metric), null)
  distance                        = try(tonumber(data.consul_keys.ospf_settings.var.distance), null)
  distance_external               = try(tonumber(data.consul_keys.ospf_settings.var.distance_external), null)
  distance_inter_area             = try(tonumber(data.consul_keys.ospf_settings.var.distance_inter_area), null)
  distance_intra_area             = try(tonumber(data.consul_keys.ospf_settings.var.distance_intra_area), null)
  distribute_list_in              = data.consul_keys.ospf_settings.var.distribute_list_in
  distribute_route_map_in         = data.consul_keys.ospf_settings.var.distribute_route_map_in
  interfaces                      = local.ospf_interfaces
  log_neighbour_changes           = data.consul_keys.ospf_settings.var.log_neighbour_changes == "" ? null : data.consul_keys.ospf_settings.var.log_neighbour_changes
  networks                        = local.ospf_networks
  redist_bgp_metric               = data.consul_keys.ospf_settings.var.redist_bgp_metric
  redist_bgp_metric_type          = data.consul_keys.ospf_settings.var.redist_bgp_metric_type
  redist_bgp_route_map            = data.consul_keys.ospf_settings.var.redist_bgp_route_map
  redist_bgp_status               = data.consul_keys.ospf_settings.var.redist_bgp_status
  redist_connected_metric         = data.consul_keys.ospf_settings.var.redist_connected_metric
  redist_connected_metric_type    = data.consul_keys.ospf_settings.var.redist_connected_metric_type
  redist_connected_route_map      = data.consul_keys.ospf_settings.var.redist_connected_route_map
  redist_connected_status         = data.consul_keys.ospf_settings.var.redist_connected_status
  redist_isis_metric              = data.consul_keys.ospf_settings.var.redist_isis_metric
  redist_isis_metric_type         = data.consul_keys.ospf_settings.var.redist_isis_metric_type
  redist_isis_route_map           = data.consul_keys.ospf_settings.var.redist_isis_route_map
  redist_isis_status              = data.consul_keys.ospf_settings.var.redist_isis_status
  redist_rip_metric               = data.consul_keys.ospf_settings.var.redist_rip_metric
  redist_rip_metric_type          = data.consul_keys.ospf_settings.var.redist_rip_metric_type
  redist_rip_route_map            = data.consul_keys.ospf_settings.var.redist_rip_route_map
  redist_rip_status               = data.consul_keys.ospf_settings.var.redist_rip_status
  redist_static_metric            = data.consul_keys.ospf_settings.var.redist_static_metric
  redist_static_metric_type       = data.consul_keys.ospf_settings.var.redist_static_metric_type
  redist_static_route_map         = data.consul_keys.ospf_settings.var.redist_static_route_map
  redist_static_status            = data.consul_keys.ospf_settings.var.redist_static_status
  restart_mode                    = data.consul_keys.ospf_settings.var.restart_mode == "" ? null : data.consul_keys.ospf_settings.var.restart_mode
  restart_period                  = try(tonumber(data.consul_keys.ospf_settings.var.restart_period), null)
  router_id                       = data.consul_keys.ospf_settings.var.router_id
}

module "bgp" {
  source = "../../modules/terraform-fortios-bgp"
  providers = {
    fortios = fortios.vdom
  }

  additional_path_select     = try(tonumber(data.consul_keys.bgp_settings.var.additional_path_select), null)
  as                         = data.consul_keys.bgp_settings.var.as
  distance_external          = try(tonumber(data.consul_keys.bgp_settings.var.distance_external), null)
  distance_internal          = try(tonumber(data.consul_keys.bgp_settings.var.distance_internal), null)
  distance_local             = try(tonumber(data.consul_keys.bgp_settings.var.distance_local), null)
  ebgp_multipath             = data.consul_keys.bgp_settings.var.ebgp_multipath == "" ? null : data.consul_keys.bgp_settings.var.ebgp_multipath
  graceful_restart           = data.consul_keys.bgp_settings.var.graceful_restart == "" ? null : data.consul_keys.bgp_settings.var.graceful_restart
  ibgp_multipath             = data.consul_keys.bgp_settings.var.ibgp_multipath == "" ? null : data.consul_keys.bgp_settings.var.ibgp_multipath
  log_neighbour_changes      = data.consul_keys.bgp_settings.var.log_neighbour_changes == "" ? null : data.consul_keys.bgp_settings.var.log_neighbour_changes
  neighbors                  = local.bgp_neighbors
  network_import_check       = data.consul_keys.bgp_settings.var.network_import_check == "" ? null : data.consul_keys.bgp_settings.var.network_import_check
  networks                   = local.bgp_networks
  recursive_next_hop         = data.consul_keys.bgp_settings.var.recursive_next_hop == "" ? null : data.consul_keys.bgp_settings.var.recursive_next_hop
  redist_connected_route_map = data.consul_keys.bgp_settings.var.redist_connected_route_map
  redist_connected_status    = data.consul_keys.bgp_settings.var.redist_connected_status
  redist_isis_route_map      = data.consul_keys.bgp_settings.var.redist_isis_route_map
  redist_isis_status         = data.consul_keys.bgp_settings.var.redist_isis_status
  redist_ospf_route_map      = data.consul_keys.bgp_settings.var.redist_ospf_route_map
  redist_ospf_status         = data.consul_keys.bgp_settings.var.redist_ospf_status
  redist_rip_route_map       = data.consul_keys.bgp_settings.var.redist_rip_route_map
  redist_rip_status          = data.consul_keys.bgp_settings.var.redist_rip_status
  redist_static_route_map    = data.consul_keys.bgp_settings.var.redist_static_route_map
  redist_static_status       = data.consul_keys.bgp_settings.var.redist_static_status
  router_id                  = data.consul_keys.bgp_settings.var.router_id
  synchronization            = data.consul_keys.bgp_settings.var.synchronization == "" ? null : data.consul_keys.bgp_settings.var.synchronization
}
