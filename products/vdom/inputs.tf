### Defaults // Common

data "consul_keys" "global_defaults" {
  key {
    name = "defaults"
    path = "iacdemo/configs/defaults/globals"
  }
}

data "consul_keys" "vdom_defaults" {
  key {
    name = "defaults"
    path = "iacdemo/configs/defaults/vdom"
  }
}

locals {
  shared = {
    managed_by_terraform_warning = jsondecode(data.consul_keys.global_defaults.var.defaults)["managed_by_terraform_warning"]
  }
}

### VDOM

data "consul_keys" "vdom" {
  key {
    name    = "install_global_objects"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/vdom_settings/install_global_objects"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["vdom_settings"]["install_global_objects"]
  }
  key {
    name    = "vcluster_id"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/vdom_settings/vcluster_id"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["vdom_settings"]["vcluster_id"]
  }
  # Not used. Due to FortiOS provider limitations vdom name MUST be terraform.workspace
  # key {
  #   name    = "vdom_name"
  #   path    = "iacdemo/configs/${terraform.workspace}/vdom/vdom_settings/vdom_name"
  #   default = null
  # }
}

### Global objects

locals {
  global_objects = [for object in jsondecode(data.consul_keys.global_defaults.var.defaults)["objects"] : merge({ comment : local.shared.managed_by_terraform_warning }, object)]
}

### Interfaces
## Loopback interfaces

data "consul_key_prefix" "loopbacks" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/"
}

data "consul_keys" "loopbacks" {
  for_each = toset([for path in(keys(data.consul_key_prefix.loopbacks.subkeys)) : split("/", path)[0]])
  key {
    name    = "allowaccess"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/allowaccess"
    default = null
  }
  key {
    name    = "description"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/description"
    default = jsondecode(data.consul_keys.global_defaults.var.defaults)["managed_by_terraform_warning"]
  }
  key {
    name    = "ipv4"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/ipv4"
    default = "0.0.0.0/0"
  }
  key {
    name    = "ipv6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/ipv6"
    default = "::/0"
  }
  key {
    name    = "is_ospf_interface"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/is_ospf_interface"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["loopbacks"]["is_ospf_interface"]
  }
  key {
    name    = "is_bgp_network"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/is_bgp_network"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["loopbacks"]["is_bgp_network"]
  }
  key {
    name    = "is_router_id"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/is_router_id"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["loopbacks"]["is_router_id"]
  }
  key {
    name    = "is_vpn_terminator"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/is_vpn_terminator"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["loopbacks"]["is_vpn_terminator"]
  }
  key {
    name    = "name"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/name"
    default = null
  }
  key {
    name    = "secondary_ip"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/secondary_ip"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["loopbacks"]["secondary_ip"]
  }
  key {
    name    = "secondary_ips"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/loopbacks/${each.key}/secondary_ips"
    default = "[]"
  }
}

# Prepend VDOM name to interface name

locals {
  loopbacks = { for interface in data.consul_keys.loopbacks : "${module.vdom.vdom.name}-${interface.var.name}" => {
    allowaccess       = interface.var.allowaccess
    description       = interface.var.description
    interface_type    = "loopback"
    ipv4              = interface.var.ipv4
    ipv6              = interface.var.ipv6
    is_bgp_network    = interface.var.is_bgp_network
    is_ospf_interface = interface.var.is_ospf_interface
    is_router_id      = interface.var.is_router_id
    is_vpn_terminator = interface.var.is_vpn_terminator
    is_zone_member    = false
    name              = "${module.vdom.vdom.name}-${interface.var.name}"
    secondary_ip      = interface.var.secondary_ip
    secondary_ips     = jsondecode(interface.var.secondary_ips)
    vdom              = module.vdom.vdom.name
    zone              = ""
    }
  }
}

## VLAN interfaces

data "consul_key_prefix" "vlans" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/"
}

data "consul_keys" "vlans" {
  for_each = toset([for path in(keys(data.consul_key_prefix.vlans.subkeys)) : split("/", path)[0]])
  key {
    name    = "allowaccess"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/allowaccess"
    default = null
  }
  key {
    name    = "aci_vrf"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/aci_vrf"
    default = terraform.workspace
  }
  key {
    name    = "description"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/description"
    default = jsondecode(data.consul_keys.global_defaults.var.defaults)["managed_by_terraform_warning"]
  }
  key {
    name    = "ipv4_dynamic"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/ipv4_dynamic"
    default = false
  }
  key {
    name    = "ipv4"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/ipv4"
    default = "0.0.0.0/0"
  }
  key {
    name    = "ipv6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/ipv6"
    default = "::/0"
  }
  key {
    name    = "is_bgp_network"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/is_bgp_network"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vlans"]["is_bgp_network"]
  }
  key {
    name    = "is_gateway_interface"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/is_gateway_interface"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vlans"]["is_gateway_interface"]
  }
  key {
    name    = "is_ospf_interface"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/is_ospf_interface"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vlans"]["is_ospf_interface"]
  }
  key {
    name    = "is_transit_interface"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/is_transit_interface"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vlans"]["is_transit_interface"]
  }
  key {
    name    = "is_zone_member"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/is_zone_member"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vlans"]["is_zone_member"]
  }
  key {
    name    = "mtu"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/mtu"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vlans"]["mtu"]
  }
  key {
    name    = "name"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/name"
    default = null
  }
  key {
    name    = "parent_interface"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/parent_interface"
    default = null
  }
  key {
    name    = "secondary_ip"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/secondary_ip"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vlans"]["secondary_ip"]
  }
  key {
    name    = "secondary_ips"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/secondary_ips"
    default = "[]"
  }
  key {
    name    = "vlanid"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/vlanid"
    default = null
  }
  key {
    name    = "zone"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vlans/${each.key}/zone"
    default = null
  }
}

# Prepend VDOM name to interface name

locals {
  vlans = { for interface in data.consul_keys.vlans : "${module.vdom.vdom.name}-${interface.var.name}" => {
    aci_vrf              = interface.var.aci_vrf
    allowaccess          = interface.var.allowaccess
    description          = interface.var.description
    interface_type       = "vlan"
    ipv4                 = tobool(interface.var.ipv4_dynamic) == true ? "${cidrhost(interface.var.ipv4, 1)}/${split("/", interface.var.ipv4)[1]}" : interface.var.ipv4
    ipv4_prefix_length   = split("/", interface.var.ipv4)[1]
    ipv6                 = interface.var.ipv6
    is_bgp_network       = interface.var.is_bgp_network
    is_gateway_interface = interface.var.is_gateway_interface
    is_ospf_interface    = interface.var.is_ospf_interface
    is_transit_interface = interface.var.is_transit_interface
    is_zone_member       = interface.var.is_zone_member
    mtu                  = interface.var.mtu
    name                 = "${module.vdom.vdom.name}-${interface.var.name}"
    parent_interface     = interface.var.parent_interface
    secondary_ip         = interface.var.secondary_ip
    secondary_ips        = jsondecode(interface.var.secondary_ips)
    vdom                 = module.vdom.vdom.name
    vlanid               = interface.var.vlanid
    zone                 = interface.var.zone
    }
  }
}

# Combine input locals

locals {
  interfaces_input = merge(local.loopbacks, local.vlans, local.vpns)
}

## VPN interfaces

data "consul_key_prefix" "vpns" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/"
}

data "consul_keys" "vpns" {
  for_each = toset([for path in(keys(data.consul_key_prefix.vpns.subkeys)) : split("/", path)[0]])
  key {
    name    = "allowaccess"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/allowaccess"
    default = null
  }
  key {
    name    = "comments"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/comments"
    default = jsondecode(data.consul_keys.global_defaults.var.defaults)["managed_by_terraform_warning"]
  }
  key {
    name    = "interface_tcp_mss"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/interface_tcp_mss"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["tcp_mss"]
  }
  key {
    name    = "is_zone_member"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/is_zone_member"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["is_zone_member"]
  }
  key {
    name    = "ip_fragmentation"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/ip_fragmentation"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["ip_fragmentation"]
  }
  key {
    name    = "ip_version"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/ip_version"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["ip_version"]
  }
  key {
    name    = "interface_local_ip"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/interface_local_ip"
    default = null
  }
  key {
    name    = "interface_remote_ip"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/interface_remote_ip"
    default = null
  }
  key {
    name    = "local_peer_ip"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/local_peer_ip"
    default = null
  }
  key {
    name    = "local_peer_ipv6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/local_peer_ipv6"
    default = null
  }
  key {
    name    = "name"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/name"
    default = null
  }
  key {
    name    = "parent_interface"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/parent_interface"
    default = null
  }
  key {
    name    = "phase1_dhgrp"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase1_dhgrp"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase1"]["dhgrp"]
  }
  key {
    name    = "phase1_dpd"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase1_dpd"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase1"]["dpd"]
  }
  key {
    name    = "phase1_ike_version"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase1_ike_version"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase1"]["ike_version"]
  }
  key {
    name    = "phase1_keylife"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase1_keylife"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase1"]["keylife"]
  }
  key {
    name    = "phase1_nattraversal"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase1_nattraversal"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase1"]["nattraversal"]
  }
  key {
    name    = "phase1_proposal"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase1_proposal"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase1"]["proposal"]
  }
  key {
    name    = "phase2_auto_negotiate"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase2_auto_negotiate"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase2"]["auto_negotiate"]
  }
  key {
    name    = "phase2_dhgrp"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase2_dhgrp"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase2"]["dhgrp"]
  }
  key {
    name    = "phase2_keepalive"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase2_keepalive"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase2"]["keepalive"]
  }
  key {
    name    = "phase2_keylife_type"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase2_keylife_type"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase2"]["keylife_type"]
  }
  key {
    name    = "phase2_keylifekbs"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase2_keylifekbs"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase2"]["keylifekbs"]
  }
  key {
    name    = "phase2_keylifeseconds"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase2_keylifeseconds"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase2"]["keylifeseconds"]
  }
  key {
    name    = "phase2_pfs"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase2_pfs"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase2"]["pfs"]
  }
  key {
    name    = "phase2_proposal"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase2_proposal"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase2"]["proposal"]
  }
  key {
    name    = "phase2_replay"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase2_replay"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["vpns"]["phase2"]["replay"]
  }
  key {
    name    = "phase2_selectors"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/phase2_selectors"
    default = null
  }
  key {
    name    = "psk"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/psk"
    default = null
  }
  key {
    name    = "remote_peer_ip"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/remote_peer_ip"
    default = null
  }
  key {
    name    = "zone"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/vpns/${each.key}/zone"
    default = null
  }
}

locals {
  vpns = { for vpn in data.consul_keys.vpns : "${module.vdom.vdom.name}-${vpn.var.name}" => {
    comments              = vpn.var.comments
    local_peer_ip         = vpn.var.local_peer_ip == "" ? null : vpn.var.local_peer_ip
    local_peer_ipv6       = vpn.var.local_peer_ipv6 == "" ? null : vpn.var.local_peer_ipv6
    interface_local_ip    = vpn.var.interface_local_ip == "" ? null : vpn.var.interface_local_ip
    interface_remote_ip   = vpn.var.interface_remote_ip == "" ? null : vpn.var.interface_remote_ip
    interface_tcp_mss     = vpn.var.interface_tcp_mss
    interface_type        = "vpn"
    ip_fragmentation      = vpn.var.ip_fragmentation
    ip_version            = vpn.var.ip_version
    is_zone_member        = vpn.var.is_zone_member
    name                  = "${module.vdom.vdom.name}-${vpn.var.name}"
    parent_interface      = "${module.vdom.vdom.name}-${vpn.var.parent_interface}"
    phase1_dpd            = vpn.var.phase1_dpd
    phase1_dhgrp          = toset(split(" ", vpn.var.phase1_dhgrp))
    phase1_ike_version    = vpn.var.phase1_ike_version
    phase1_keylife        = vpn.var.phase1_keylife
    phase1_nattraversal   = vpn.var.phase1_nattraversal
    phase1_proposal       = toset(split(" ", vpn.var.phase1_proposal))
    phase2_auto_negotiate = vpn.var.phase2_auto_negotiate
    phase2_dhgrp          = toset(split(" ", vpn.var.phase2_dhgrp))
    phase2_keepalive      = vpn.var.phase2_keepalive
    phase2_keylife_type   = vpn.var.phase2_keylife_type
    phase2_keylifekbs     = vpn.var.phase2_keylifekbs
    phase2_keylifeseconds = vpn.var.phase2_keylifeseconds
    phase2_pfs            = vpn.var.phase2_pfs
    phase2_proposal       = toset(split(" ", vpn.var.phase2_proposal))
    phase2_replay         = vpn.var.phase2_replay
    phase2_selectors      = vpn.var.phase2_selectors == "" ? null : jsondecode(vpn.var.phase2_selectors)
    psk                   = vpn.var.psk
    remote_peer_ip        = vpn.var.remote_peer_ip
    vdom                  = module.vdom.vdom.name
    zone                  = vpn.var.zone
    }
  }
}

## Zones

# Get list of defined zones

data "consul_key_prefix" "zones" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/zones/"
}

locals {
  zones_interfaces = [for interface in local.interfaces_input : interface.zone]
  # zones_vpns       = [for vpn in local.vpns : vpn.zone]
  # zones_interfaces = concat(local.zones_vlans, local.zones_vpns)
  zones_consul   = [for path in(keys(data.consul_key_prefix.zones.subkeys)) : split("/", path)[0]]
  zones_combined = compact(distinct(flatten([local.zones_interfaces, local.zones_consul])))
}

data "consul_keys" "zones" {
  for_each = toset(local.zones_combined)
  key {
    name    = "intrazone_traffic"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/zones/${each.key}/intrazone_traffic"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["interfaces"]["zones"]["intrazone_traffic"]
  }
  key {
    name    = "members"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/interfaces/zones/${each.key}/members"
    default = "[]"
  }
}

# Zone object

locals {
  zones = { for k, v in data.consul_keys.zones : k => {
    intrazone_traffic = v.var.intrazone_traffic
    name              = k
    members           = compact(flatten([[for i in jsondecode(v.var.members) : lookup(local.interfaces_output["${module.vdom.vdom.name}-${i}"], "name", [])], [for interface in local.interfaces_input : interface.name if interface.zone == k && interface.is_zone_member]]))
  } }
}

### Routing

# Get IP of loopbacks flagged as routerid (only first is selected)

locals {
  router_id_loopbacks = [for x in module.loopbacks : split("/", module.loopbacks[x.interface.name].primary_ip)[0] if local.loopbacks[x.interface.name].is_router_id]
  router_id           = length(local.router_id_loopbacks) == 0 ? null : local.router_id_loopbacks[0]
}

## Static routes

# Get configured static routes from consul and set defaults

data "consul_key_prefix" "static" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/"
}

data "consul_keys" "static" {
  for_each = toset([for path in(keys(data.consul_key_prefix.static.subkeys)) : split("/", path)[0]])
  key {
    name    = "blackhole"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/blackhole"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["static"]["routes"]["blackhole"]
  }
  key {
    name    = "comment"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/comment"
    default = jsondecode(data.consul_keys.global_defaults.var.defaults)["managed_by_terraform_warning"]
  }
  key {
    name    = "device"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/device"
    default = null
  }
  key {
    name    = "distance"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/distance"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["static"]["routes"]["distance"]
  }
  key {
    name    = "dst"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/dst"
    default = null
  }
  key {
    name    = "gateway"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/gateway"
    default = null
  }
  key {
    name    = "is_ipv6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/is_ipv6"
    default = false
  }
  key {
    name    = "priority"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/priority"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["static"]["routes"]["priority"]
  }
  key {
    name    = "sdwan"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/sdwan"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["static"]["routes"]["sdwan"]
  }
  key {
    name    = "status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["static"]["routes"]["status"]
  }
  # This key is not used in 6.4 perhaps introduced in 7.0?
  # key {
  #   name    = "vrf"
  #   path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/static/routes/${each.key}/vrf"
  #   default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["static"]["routes"]["vrf"]
  # }
}

locals {
  # Final object creation for feeding to config module
  ipv4_static_routes = { for k, route in data.consul_keys.static : k => {
    blackhole = route.var.blackhole
    comment   = route.var.comment
    device    = route.var.device == "" ? null : route.var.device == "ssl.${module.vdom.vdom.name}" ? "ssl.${module.vdom.vdom.name}" : local.interfaces_output["${module.vdom.vdom.name}-${route.var.device}"].name
    distance  = route.var.distance
    dst       = route.var.dst
    gateway   = route.var.gateway == "" ? null : route.var.gateway
    priority  = route.var.priority == "0" ? null : route.var.priority
    status    = route.var.status
    sdwan     = route.var.sdwan
    } if !route.var.is_ipv6
  }
  ipv6_static_routes = { for k, route in data.consul_keys.static : k => {
    blackhole = route.var.blackhole
    comment   = route.var.comment
    device    = route.var.device == "" ? null : route.var.device == "ssl.${module.vdom.vdom.name}" ? "ssl.${module.vdom.vdom.name}" : local.interfaces_output["${module.vdom.vdom.name}-${route.var.device}"].name
    distance  = route.var.distance
    dst       = route.var.dst
    gateway   = route.var.gateway == "" ? null : route.var.gateway
    priority  = route.var.priority == "0" ? null : route.var.priority
    status    = route.var.status
    sdwan     = route.var.sdwan
    } if route.var.is_ipv6
  }
}

## OSPF

# Set router_id if manually defined, else pick value from flagged interface, else default

data "consul_keys" "ospf_settings" {
  key {
    name    = "bfd"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/bfd"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["bfd"]
  }
  key {
    name    = "default_information_metric"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/default_information_metric"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["default_information_metric"]
  }
  key {
    name    = "default_information_metric_type"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/default_information_metric_type"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["default_information_metric_type"]
  }
  key {
    name    = "default_information_originate"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/default_information_originate"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["default_information_originate"]
  }
  key {
    name    = "default_metric"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/default_metric"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["default_metric"]
  }
  key {
    name    = "distance"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/distance"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["distance"]
  }
  key {
    name    = "distance_external"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/distance_external"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["distance_external"]
  }
  key {
    name    = "distance_inter_area"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/distance_inter_area"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["distance_inter_area"]
  }
  key {
    name    = "distance_intra_area"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/distance_intra_area"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["distance_intra_area"]
  }
  key {
    name    = "distribute_list_in"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/distribute_list_in"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["distribute_list_in"]
  }
  key {
    name    = "distribute_route_map_in"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/distribute_route_map_in"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["distribute_route_map_in"]
  }
  key {
    name    = "log_neighbour_changes"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/log_neighbour_changes"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["log_neighbour_changes"]
  }
  key {
    name    = "redist_bgp_metric"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/bgp/metric"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["bgp"]["metric"]
  }
  key {
    name    = "redist_bgp_metric_type"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/bgp/metric_type"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["bgp"]["metric_type"]
  }
  key {
    name    = "redist_bgp_route_map"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/bgp/route_map"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["bgp"]["route_map"]
  }
  key {
    name    = "redist_bgp_status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/bgp/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["bgp"]["status"]
  }
  key {
    name    = "redist_connected_metric"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/connected/metric"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["connected"]["metric"]
  }
  key {
    name    = "redist_connected_metric_type"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/connected/metric_type"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["connected"]["metric_type"]
  }
  key {
    name    = "redist_connected_route_map"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/connected/route_map"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["connected"]["route_map"]
  }
  key {
    name    = "redist_connected_status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/connected/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["connected"]["status"]
  }
  key {
    name    = "redist_isis_metric"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/isis/metric"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["isis"]["metric"]
  }
  key {
    name    = "redist_isis_metric_type"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/isis/metric_type"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["isis"]["metric_type"]
  }
  key {
    name    = "redist_isis_route_map"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/isis/route_map"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["isis"]["route_map"]
  }
  key {
    name    = "redist_isis_status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/isis/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["isis"]["status"]
  }
  key {
    name    = "redist_rip_metric"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/rip/metric"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["rip"]["metric"]
  }
  key {
    name    = "redist_rip_metric_type"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/rip/metric_type"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["rip"]["metric_type"]
  }
  key {
    name    = "redist_rip_route_map"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/rip/route_map"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["rip"]["route_map"]
  }
  key {
    name    = "redist_rip_status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/rip/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["rip"]["status"]
  }
  key {
    name    = "redist_static_metric"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/static/metric"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["static"]["metric"]
  }
  key {
    name    = "redist_static_metric_type"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/static/metric_type"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["static"]["metric_type"]
  }
  key {
    name    = "redist_static_route_map"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/static/route_map"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["static"]["route_map"]
  }
  key {
    name    = "redist_static_status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/redistribute/static/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["redistribute"]["static"]["status"]
  }
  key {
    name    = "restart_mode"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/restart_mode"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["restart_mode"]
  }
  key {
    name    = "restart_period"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/restart_period"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["restart_period"]
  }
  key {
    name    = "router_id"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/settings/router_id"
    default = local.router_id == null ? jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["settings"]["router_id"] : local.router_id
  }
}

# Get manually configured networks

data "consul_key_prefix" "ospf_networks" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/networks/"
}

# Combine manual with flagged interfaces

data "cidr_network" "ospf_networks_loopbacks" {
  for_each = toset([for x in module.loopbacks : module.loopbacks[x.interface.name].primary_ip if local.loopbacks[x.interface.name].is_ospf_interface])

  prefix = each.key
}

data "cidr_network" "ospf_networks_vlans" {
  for_each = toset([for x in module.vlans : module.vlans[x.interface.name].primary_ip if local.vlans[x.interface.name].is_ospf_interface])

  prefix = each.key
}

locals {
  ospf_networks_loopbacks = [for x in data.cidr_network.ospf_networks_loopbacks : x.network]
  ospf_networks_vlans     = [for x in data.cidr_network.ospf_networks_vlans : x.network]
  ospf_networks_consul    = [for path in(keys(data.consul_key_prefix.ospf_networks.subkeys)) : split("/", path)[0]]
  ospf_networks_combined  = setunion(local.ospf_networks_loopbacks, local.ospf_networks_vlans, local.ospf_networks_consul)
}

data "consul_keys" "ospf_networks" {
  for_each = local.ospf_networks_combined
  key {
    name    = "area"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/networks/${each.key}/area"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["networks"]["area"]
  }
  key {
    name    = "network"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/networks/${each.key}/network"
    default = each.key
  }
}

locals {
  # Final object creation for feeding to config module
  ospf_networks = distinct([for network in data.consul_keys.ospf_networks : network.var])
}

# Get manually configured interfaces

data "consul_key_prefix" "ospf_interfaces" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/interfaces/"
}

# Combine manual and flagged interfaces

locals {
  ospf_interfaces_loopbacks = [for x in module.loopbacks : x.interface.name if local.loopbacks[x.interface.name].is_ospf_interface]
  ospf_interfaces_vlans     = [for x in module.vlans : x.interface.name if local.vlans[x.interface.name].is_ospf_interface]
  ospf_interfaces_consul    = [for path in(keys(data.consul_key_prefix.ospf_interfaces.subkeys)) : split("/", path)[0]]
  ospf_interfaces_combined  = setunion(local.ospf_interfaces_loopbacks, local.ospf_interfaces_vlans, local.ospf_interfaces_consul)
}

data "consul_keys" "ospf_interfaces" {
  for_each = local.ospf_interfaces_combined
  key {
    name    = "authentication"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/interfaces/${each.key}/authentication"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["interfaces"]["authentication"]
  }
  key {
    name    = "authentication_key"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/interfaces/${each.key}/authentication_key"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["interfaces"]["authentication_key"]
  }
  key {
    name    = "cost"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/interfaces/${each.key}/cost"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["ospf"]["interfaces"]["cost"]
  }
  key {
    name    = "interface"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/interfaces/${each.key}/interface"
    default = each.key
  }
  key {
    name    = "name"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/ospf/interfaces/${each.key}/name"
    default = each.key
  }
}

locals {
  # Final object creation for feeding to config module
  ospf_interfaces = [for ospfint in data.consul_keys.ospf_interfaces : {
    authentication     = ospfint.var.authentication
    authentication_key = ospfint.var.authentication_key
    cost               = ospfint.var.cost
    interface          = "${module.vdom.vdom.name}-${trimprefix(ospfint.var.interface, "${module.vdom.vdom.name}-")}"
    name               = ospfint.var.name
    }
  ]
}

## BGP

# Set router_id if manually defined, else pick value from flagged interface, else default

data "consul_keys" "bgp_settings" {
  key {
    name    = "additional_path_select"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/additional_path_select"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["additional_path_select"]
  }
  key {
    name    = "as"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/as"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["as"]
  }
  key {
    name    = "distance_external"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/distance_external"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["distance_external"]
  }
  key {
    name    = "distance_internal"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/distance_internal"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["distance_internal"]
  }
  key {
    name    = "distance_local"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/distance_local"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["distance_local"]
  }
  key {
    name    = "ebgp_multipath"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/ebgp_multipath"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["ebgp_multipath"]
  }
  key {
    name    = "graceful_restart"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/graceful_restart"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["graceful_restart"]
  }
  key {
    name    = "ibgp_multipath"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/ibgp_multipath"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["ibgp_multipath"]
  }
  key {
    name    = "log_neighbour_changes"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/log_neighbour_changes"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["log_neighbour_changes"]
  }
  key {
    name    = "network_import_check"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/network_import_check"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["network_import_check"]
  }
  key {
    name    = "recursive_next_hop"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/recursive_next_hop"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["recursive_next_hop"]
  }
  key {
    name    = "router_id"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/router_id"
    default = local.router_id == null ? jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["router_id"] : local.router_id
  }
  key {
    name    = "redist_connected_status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/redistribute/connected/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["redistribute"]["connected"]["status"]
  }
  key {
    name    = "redist_connected_route_map"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/redistribute/connected/route_map"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["redistribute"]["connected"]["route_map"]
  }
  key {
    name    = "redist_isis_status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/redistribute/isis/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["redistribute"]["isis"]["status"]
  }
  key {
    name    = "redist_isis_route_map"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/redistribute/isis/route_map"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["redistribute"]["isis"]["route_map"]
  }
  key {
    name    = "redist_ospf_status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/redistribute/ospf/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["redistribute"]["ospf"]["status"]
  }
  key {
    name    = "redist_ospf_route_map"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/redistribute/ospf/route_map"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["redistribute"]["ospf"]["route_map"]
  }
  key {
    name    = "redist_rip_status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/redistribute/rip/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["redistribute"]["rip"]["status"]
  }
  key {
    name    = "redist_rip_route_map"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/redistribute/rip/route_map"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["redistribute"]["rip"]["route_map"]
  }
  key {
    name    = "redist_static_status"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/redistribute/static/status"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["redistribute"]["static"]["status"]
  }
  key {
    name    = "redist_static_route_map"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/redistribute/static/route_map"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["redistribute"]["static"]["route_map"]
  }
  key {
    name    = "synchronization"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/settings/synchronization"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["settings"]["synchronization"]
  }
}

# Get manually configured networks

data "consul_key_prefix" "bgp_networks" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/networks/"
}

# Combine manual with flagged interfaces

data "cidr_network" "bgp_networks_loopbacks" {
  for_each = toset([for x in module.loopbacks : module.loopbacks[x.interface.name].primary_ip if local.loopbacks[x.interface.name].is_bgp_network])

  prefix = each.key
}

data "cidr_network" "bgp_networks_vlans" {
  for_each = toset([for x in module.vlans : module.vlans[x.interface.name].primary_ip if local.vlans[x.interface.name].is_bgp_network])

  prefix = each.key
}

locals {
  bgp_networks_loopbacks = [for x in data.cidr_network.bgp_networks_loopbacks : x.network]
  bgp_networks_vlans     = [for x in data.cidr_network.bgp_networks_vlans : x.network]
  bgp_networks_consul    = [for path in(keys(data.consul_key_prefix.bgp_networks.subkeys)) : split("/", path)[0]]
  bgp_networks_combined  = setunion(local.bgp_networks_loopbacks, local.bgp_networks_vlans, local.bgp_networks_consul)
}

data "consul_keys" "bgp_networks" {
  for_each = local.bgp_networks_combined
  key {
    name    = "network"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/networks/${each.key}/network"
    default = each.key
  }
}

locals {
  # Final object creation for feeding to config module
  bgp_networks = distinct([for network in data.consul_keys.bgp_networks : network.var])
}

# Get manually configured neighbors

data "consul_key_prefix" "bgp_neighbors" {
  path_prefix = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/"
}

data "consul_keys" "bgp_neighbors" {
  for_each = toset([for path in(keys(data.consul_key_prefix.bgp_neighbors.subkeys)) : split("/", path)[0]])
  key {
    name    = "activate"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/activate"
    default = null
  }
  key {
    name    = "activate6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/activate6"
    default = null
  }
  key {
    name    = "additional_path"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/additional_path"
    default = null
  }
  key {
    name    = "additional_path6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/additional_path6"
    default = null
  }
  key {
    name    = "capability_default_originate"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/capability_default_originate"
    default = null
  }
  key {
    name    = "capability_default_originate6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/capability_default_originate6"
    default = null
  }
  key {
    name    = "capability_graceful_restart"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/capability_graceful_restart"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["neighbors"]["capability_graceful_restart"]
  }
  key {
    name    = "capability_graceful_restart6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/capability_graceful_restart6"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["neighbors"]["capability_graceful_restart6"]
  }
  key {
    name    = "default_originate_routemap"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/default_originate_routemap"
    default = null
  }
  key {
    name    = "default_originate_routemap6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/default_originate_routemap6"
    default = null
  }
  key {
    name    = "description"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/description"
    default = local.shared.managed_by_terraform_warning
  }
  key {
    name    = "distribute_list_in"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/distribute_list_in"
    default = null
  }
  key {
    name    = "distribute_list_in6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/distribute_list_in6"
    default = null
  }
  key {
    name    = "distribute_list_out"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/distribute_list_out"
    default = null
  }
  key {
    name    = "distribute_list_out6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/distribute_list_out6"
    default = null
  }
  key {
    name    = "ebgp_enforce_multihop"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/ebgp_enforce_multihop"
    default = null
  }
  key {
    name    = "filter_list_in"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/filter_list_in"
    default = null
  }
  key {
    name    = "filter_list_in6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/filter_list_in6"
    default = null
  }
  key {
    name    = "filter_list_out"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/filter_list_out"
    default = null
  }
  key {
    name    = "filter_list_out6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/filter_list_out6"
    default = null
  }
  key {
    name    = "interface"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/interface"
    default = null
  }
  key {
    name    = "ip"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/ip"
    default = null
  }
  key {
    name    = "local_as"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/local_as"
    default = null
  }
  key {
    name    = "maximum_prefix"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/maximum_prefix"
    default = null
  }
  key {
    name    = "maximum_prefix6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/maximum_prefix6"
    default = null
  }
  key {
    name    = "maximum_prefix_threshold"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/maximum_prefix_threshold"
    default = null
  }
  key {
    name    = "maximum_prefix_threshold6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/maximum_prefix_threshold6"
    default = null
  }
  key {
    name    = "maximum_prefix_warning_only"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/maximum_prefix_warning_only"
    default = null
  }
  key {
    name    = "maximum_prefix_warning_only6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/maximum_prefix_warning_only6"
    default = null
  }
  key {
    name    = "next_hop_self"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/next_hop_self"
    default = null
  }
  key {
    name    = "next_hop_self6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/next_hop_self6"
    default = null
  }
  key {
    name    = "next_hop_self_rr"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/next_hop_self_rr"
    default = null
  }
  key {
    name    = "next_hop_self_rr6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/next_hop_self_rr6"
    default = null
  }
  key {
    name    = "password"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/password"
    default = null
  }
  key {
    name    = "prefix_list_in"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/prefix_list_in"
    default = null
  }
  key {
    name    = "prefix_list_in6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/prefix_list_in6"
    default = null
  }
  key {
    name    = "prefix_list_out"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/prefix_list_out"
    default = null
  }
  key {
    name    = "prefix_list_out6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/prefix_list_out6"
    default = null
  }
  key {
    name    = "remote_as"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/remote_as"
    default = null
  }
  key {
    name    = "remove_private_as"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/remove_private_as"
    default = null
  }
  key {
    name    = "remove_private_as6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/remove_private_as6"
    default = null
  }
  key {
    name    = "route_map_in"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/route_map_in"
    default = null
  }
  key {
    name    = "route_map_in6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/route_map_in6"
    default = null
  }
  key {
    name    = "route_map_out"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/route_map_out"
    default = null
  }
  key {
    name    = "route_map_out6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/route_map_out6"
    default = null
  }
  key {
    name    = "route_map_out_preferable"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/route_map_out_preferable"
    default = null
  }
  key {
    name    = "route_map_out6_preferable"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/route_map_out6_preferable"
    default = null
  }
  key {
    name    = "route_reflector_client"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/route_reflector_client"
    default = null
  }
  key {
    name    = "route_reflector_client6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/route_reflector_client6"
    default = null
  }
  key {
    name    = "shutdown"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/shutdown"
    default = null
  }
  key {
    name    = "soft_reconfiguration"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/soft_reconfiguration"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["neighbors"]["soft_reconfiguration"]
  }
  key {
    name    = "soft_reconfiguration6"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/soft_reconfiguration6"
    default = jsondecode(data.consul_keys.vdom_defaults.var.defaults)["routing"]["bgp"]["neighbors"]["soft_reconfiguration6"]
  }
  key {
    name    = "update_source"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/update_source"
    default = null
  }
  key {
    name    = "weight"
    path    = "iacdemo/configs/${terraform.workspace}/vdom/routing/bgp/neighbors/${each.key}/weight"
    default = null
  }
}

locals {
  # Final object creation for feeding to config module
  bgp_neighbors = { for neighbor, values in data.consul_keys.bgp_neighbors : neighbor => {
    for k, v in values.var : k => v == "" ? null : v
    }
  }
}
