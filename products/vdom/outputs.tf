# Craft interface outputs

locals {
  loopbacks_output = { for loopback in local.loopbacks : module.loopbacks[loopback.name].interface.name => loopback }
  vlans_output     = { for vlan in local.vlans : module.vlans[vlan.name].interface.name => vlan }
  vpns_output      = { for vpn in local.vpns : module.vpns[vpn.name].interface.name => vpn }
}

# Combine interface outputs

locals {
  interfaces_output = merge(local.loopbacks_output, local.vlans_output, local.vpns_output)
}


output "vdom_name" {
  description = "Name of VDOM."
  value       = module.vdom.vdom.name
}

output "interfaces" {
  description = "Object containing interface configuration potentially of interest to other products."
  value       = local.interfaces_output
}

# output "debug" {
#   value = local.ospf_interfaces
# }