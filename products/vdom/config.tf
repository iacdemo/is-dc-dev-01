terraform {
  required_providers {
    cidr = {
      source  = "volcano-coffee-company/cidr"
      version = "0.1.0"
    }
    consul = {
      source  = "hashicorp/consul"
      version = ">= 2.11.0"
    }
    fortios = {
      source  = "poroping/fortios"
      version = ">= 2.3.1"
    }
  }
}

provider "fortios" {
  alias = "root"
}

provider "fortios" {
  alias = "vdom"
}