module "product_vdom" {
  for_each = { for vdom in [terraform.workspace] : terraform.workspace => vdom if data.consul_keys.products.var.has_vdom }
  source   = "./products/vdom"
  providers = {
    fortios.root = fortios.root
    fortios.vdom = fortios.vdom
  }
}

module "product_aci_tenant" {
  for_each = { for tenant in [terraform.workspace] : terraform.workspace => tenant if data.consul_keys.products.var.has_aci }
  source   = "./products/aci"
}

module "product_extsw_internet" {
  for_each = { for extsw_internet in [terraform.workspace] : terraform.workspace => extsw_internet if data.consul_keys.products.var.has_aci }
  source   = "./products/extsw_internet"
}