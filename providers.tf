## Shared providers

provider "consul" {
}

# provider "vault" {
# }

## Product specific providers

# vdom

provider "fortios" {
  alias    = "vdom"
  hostname = var.FGT_IAC_LAB_HOST
  token    = var.FGT_IAC_LAB_TOKEN
  vdom     = terraform.workspace
  insecure = "true"
  #   cabundlefile =  "/path/yourCA.crt"
}

# hosted_fw

provider "fortios" {
  alias    = "root"
  hostname = var.FGT_IAC_LAB_HOST
  token    = var.FGT_IAC_LAB_TOKEN
  vdom     = "root"
  insecure = "true"
  #   cabundlefile =  "/path/yourCA.crt"
}

# aci

provider "aci" {
  username = var.lab_apic_username
  #  private_key = var.lab_apic_keypath
  #  cert_name   = var.lab_apic_certname
  password = var.lab_apic_password
  url      = var.lab_apic_url
  insecure = true
}

# ext_sw

provider "iosxe" {
  insecure = true
}