terraform {
  backend "consul" {
    path = "terraform/states/dev/is-dc-dev-01"
    gzip = "true"
  }
  required_providers {
    consul = {
      source  = "hashicorp/consul"
      version = "~> 2.11.0"
    }
    fortios = {
      source  = "poroping/fortios"
      version = "~> 2.3.5"
    }
    aci = {
      source  = "CiscoDevNet/aci"
      version = "~> 0.7.1"
    }
    iosxe = {
      source  = "poroping/iosxe"
      version = "~> 0.0.1"
    }
  }
}
