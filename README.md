==Infrastructure as Code Terraform Repository==

**Datacenter: IS-DC-DEV-01**

Customer ID is used as ```terraform.workspace```

product modules under ./products

products should be enabled by setting a bool in consul at ```"${terraform.workspace}/products/has_${product}"``` the default should be ```false```.

product 'super' module handles info gathering / defaults, formatting and logic. Then passes down to submodules for actual resource creation/modifications.

each product module contains a ```defaults.json``` which will be made available in consul at ```defaults/${product}``` these defaults are used across customers


### ```config.tf```
terraform configuration block

### ```inputs.tf```
product booleans

### ```main.tf```
main file runs product modules per customer (workspace) based on bool values

### ```providers.tf```
all provider configurations, sorted into shared and per-product providers

### ```variables.tf```
where to define variables for root module

# Demo
![high-level diagram](img/hld.png "HLD")

![automated](img/automated.png "What is automated?")