variable "FGT_IAC_LAB_HOST" {
  type        = string
  description = "Management IP"
}

variable "FGT_IAC_LAB_TOKEN" {
  type        = string
  description = "API token"
  sensitive   = true
}

variable "lab_apic_url" {
  type        = string
  description = "APIC Url"
}

variable "lab_apic_username" {
  type        = string
  description = "Username to log in to apic with"
}

variable "lab_apic_password" {
  type        = string
  description = "Password for lab APIC"
}

variable "lab_apic_keypath" {
  type        = string
  description = "lab_apic key path"
  default     = "."
}

variable "lab_apic_certname" {
  type        = string
  description = "Name of the certificate as it is stored on the APIC"
  default     = "gitlab"
}