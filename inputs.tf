data "consul_keys" "products" {
  key {
    name    = "has_vdom"
    path    = "iacdemo/configs/${terraform.workspace}/products/has_vdom"
    default = false
  }
  key {
    name    = "has_aci"
    path    = "iacdemo/configs/${terraform.workspace}/products/has_aci"
    default = false
  }
  key {
    name    = "has_extsw_internet"
    path    = "iacdemo/configs/${terraform.workspace}/products/has_extsw_internet"
    default = false
  }
}